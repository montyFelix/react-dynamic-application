import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Comment extends Component {
  render() {
  	return(
    		<div className="comment">
    			<span className="avatar"></span>
    			<p className="commentBody">{this.props.comment}</p>
    		</div>
  		)
  }

 }

 Comment.propTypes = {
  comment: PropTypes.string.isRequired
}

export default Comment;
