import React, { Component } from 'react';
import PropTypes from 'prop-types';

class AddItem extends Component {
  constructor(props) {
    super(props);
    this.handleAdd = this.handleAdd.bind(this);
  }

  handleAdd(e) {
  	if (this.refs.inputItemName.value) {
  		let item = this.refs.inputItemName;
    	let tempItem = {
      		name: item.value,
      		comments: []
    	};
    	e.preventDefault();
    	item.value = "";
    	this.props.addItem(tempItem);
  	}
  	e.preventDefault();
  	
  }

  render() {
  	return (
  		<div>
	  		<form className="AddItemForm" onSubmit={this.handleAdd}>
	  			<input type="text" id="itemName" ref="inputItemName" 
	  					placeholder="Type Name here..." />
	        	<button type="submit" className="button">Add new</button>
	        </form>	
        </div>
        	)
  }

}

AddItem.propTypes = {
  addItem: PropTypes.func.isRequired
}

export default AddItem;