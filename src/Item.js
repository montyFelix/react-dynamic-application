import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Item extends Component {
  constructor(props) {
    super(props);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleDelete() {
    this.props.onDelete(this.props.whichItem)
  }

  handleClick() {
  	this.props.onClick(this.props.whichItem)
  }

  render() {
  	return(
  		<div className="ItemContainer">
	  		<div className="Item" onClick={this.handleClick}>
	  			<p className="itemSection">{this.props.name} 
	  			<span className="commentsCount">{this.props.comments.length}</span></p>
	  		</div>
	  		<button type="submit" className="deleteButton" 
	  				onClick={this.handleDelete}>Delete</button>
  		</div>
  		)
  }

 }

 Item.propTypes = {
  key: PropTypes.number.isRequired,
  whichItem: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  onDelete: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired,
  comments: PropTypes.array.isRequired
}

export default Item;
