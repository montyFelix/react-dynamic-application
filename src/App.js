import React, { Component } from 'react';
import './App.css';
import AddItem from './AddItem';
import Item from './Item';
import Comments from './Comments';

class App extends Component {
  constructor(props) {
    super(props);
    let Items = localStorage.getItem('Items');
    this.state = {
      Items:  JSON.parse(Items),
      comments: [],
      whichComments: -1
    }
    this.addItem = this.addItem.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
    this.addComment = this.addComment.bind(this);
    this.setComments = this.setComments.bind(this);
  } 

  componentWillMount() {
  }

  addItem(tempItem) {
    let tempItems = this.state.Items || [];
    tempItems.push(tempItem);
    localStorage.setItem("Items", tempItems);
    this.setState((prevState) => {
      localStorage.setItem('Items', JSON.stringify(tempItems));
      return {Items: tempItems}
    })
  }

  deleteItem(itemIndex) {
    let Items = this.state.Items;
    Items.splice(itemIndex, 1);
    this.setState((prevState) => {
      localStorage.setItem('Items', JSON.stringify(this.state.Items));
      return {
        Items: Items,
        comments: []}
    })
  }

  addComment(comment) {
    let comments = this.state.comments;
    comments.push(comment);
    this.setState((prevState) => {
      localStorage.setItem('Items', JSON.stringify(this.state.Items));
      return {comments: comments}
    })
  }

  setComments(i) {
    let items = this.state.Items;
    this.setState((prevState) => {
      return {
        comments: items[i].comments,
        whichComments: i}
    })
  }


  render() {
    let items = null;

    if(this.state.Items) {
      items = this.state.Items.map((item, i) => {
            return <Item key={i} whichItem={i} name={item.name} 
                    onDelete={this.deleteItem} onClick={this.setComments} 
                    comments={item.comments} />
          });
    }

    let comments = null;

    if (this.state.whichComments >= 0) {
            comments = <Comments AddComment={this.addComment}
                                 comments={this.state.comments}
            whichComments={this.state.whichComments} />
          }

    return (
      <div className="App">
        <div className="App-sidebar">
          <h1 className="App-title">DAIRY APP</h1>
          <p>Comment with no Sense</p>
        </div>
        <div className="Container">
          <div className="block1">
            <div className="InnerDiv">
              <h2>Items</h2>
              <AddItem addItem={this.addItem} />
              {items}
            </div>
          </div>
          <div className="block2">
            <div className="InnerDiv">
              {comments}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
