import React, { Component } from 'react';
import PropTypes from 'prop-types';

class AddComment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    }
    this.handleChange = this.handleChange.bind(this);
    this.keydownHandler = this.keydownHandler.bind(this);
  }

  keydownHandler(event){
    if(event.keyCode===13 && event.ctrlKey && this.state.value) {
      this.props.addComment(this.state.value)
      this.setState({
        value: ''
      })
    }
  }

  componentDidMount(){
    document.addEventListener('keydown', this.keydownHandler);
  }

  componentWillUnmount(){
    document.removeEventListener('keydown', this.keydownHandler);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  render() {
  	return(
  		<div className="AddComment">
  			<span className="avatar"></span>
  			<textarea className="commentArea" value={this.state.value} 
                  onChange={this.handleChange} />
  		</div>
  		)
  }

 }

 AddComment.propTypes = {
  addComment: PropTypes.func.isRequired
}

export default AddComment;
