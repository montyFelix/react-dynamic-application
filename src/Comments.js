import React, { Component } from 'react';
import Comment from './Comment';
import AddComment from './AddComment';
import PropTypes from 'prop-types';

class Comments extends Component {
  render() {
    let comments = this.props.comments ? this.props.comments.map((item, i) => {
      return <Comment key={i} comment={item} />
    }) : null

  	return(
      <div>
        <h2>Comments #{this.props.whichComments+1}</h2>
    		{comments}
        <AddComment addComment={this.props.AddComment} />
      </div>
  		)
  }

 }

Comments.propTypes = {
  comments: PropTypes.array.isRequired,
  whichComments: PropTypes.number.isRequired,
  AddComment: PropTypes.func.isRequired
}

export default Comments;